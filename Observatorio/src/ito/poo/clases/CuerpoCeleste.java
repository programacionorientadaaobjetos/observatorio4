package ito.poo.clases;

import java.util.ArrayList;

public class CuerpoCeleste {

	
	private String nombre;
	private String composicion;
	private ArrayList<Ubicacion> localizaciones;
	
	
	public CuerpoCeleste(String nombre, String composicion) {
		super();
		this.nombre = nombre;
		this.composicion = composicion;
		this.localizaciones=new ArrayList<Ubicacion>();
	}

	
	public void agregaUbicacion(Ubicacion c) {
		if(!this.localizaciones.contains(c)) {
			this.localizaciones.add(c);
		}
	}
	
	public float obtieneDistanciaEntredecuerposcelestes(int i, int j) {
		float distancia=-1;
		if(i<this.localizaciones.size() && j<this.localizaciones.size()) {
			Ubicacion ci,cj;
			ci= this.localizaciones.get(i);
			cj= this.localizaciones.get(j);
			distancia = Math.abs(ci.getDistancia()-cj.getDistancia());
			
		}
		return distancia;
	}
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getComposicion() {
		return composicion;
	}


	public void setComposicion(String composicion) {
		this.composicion = composicion;
	}


	public ArrayList<Ubicacion> getLocalizaciones() {
		return localizaciones;
	}


	@Override
	public String toString() {
		return "CuerpoCeleste [nombre=" + nombre + ", composicion=" + composicion + ", localizaciones=" + localizaciones
				+ "]";
	}
	
}
